// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FunctionLibrary/TypeUtility.h"
#include "Engine/DataTable.h"
#include "Weapon/WeaponDefault.h"
#include "TDSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	

	public:
		//Table
          UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Setting")
          UDataTable* WeaponInfoTable = nullptr;
          UFUNCTION(BlueprintCallable)
          bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
};
