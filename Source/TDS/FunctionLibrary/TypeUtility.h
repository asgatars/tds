// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"

#include "TypeUtility.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Aim_State UMETA(DisplayName = "Aim State"),
    AimWalk_State UMETA(DisplayName = "Aim State"),
    Walk_State UMETA(DisplayName = "Walk State"),
    Run_State UMETA(DisplayName = "Run State"),
    Sprint_State UMETA(DisplayName = "Run State"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeed = 400.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimWalkSpeed = 300.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float WalkSpeed = 300.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeed = 600.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float SprintSpeed = 800.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    TSubclassOf<class AProjectileDefault> Projectile = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileDamage = 20.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float LifeTime = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float InitialSpeed = 2500.0f;

    // Hit FX Actor?

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Setting")
    bool bIsLikeBomb = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Setting")
    float ProjectileMaxRadiusDamage = 200.0f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispertion")
    float DispertionAimStart = 0.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispertion")
    float DispertionAimMax = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispertion")
    float DispertionAimMin = 0.1f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispertion")
    float DispertionAimShootCoef = 1.0f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
    TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    float RateOfFire = 0.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    float ReloadTime = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    int32 MaxRound = 10;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispertion")
    FWeaponDispersion DispertionWeapon;

    // Sound Effect
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundBase* SoundFireWeapon = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundBase* SoundReloadWeapon = nullptr;

    // Visual Effect VFX
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    UParticleSystem* EffectFireWeapon = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
    FProjectileInfo ProjectileSetting;

    //Settings For Trace Shoot
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
    float WeaponDamage = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
    float DistanceTrace = 2000.0f;

    // one decal for all
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
    UDecalComponent* DecalOnHit = nullptr;

    //Animation Montage for Character
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
    UAnimMontage* AnimCharFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
    UAnimMontage* AnimCharFireAim = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
    UAnimMontage* AnimCharReload = nullptr;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dropped Magazine")
    //TSubclassOf<class ADroppableItemDefault> MagazineDrop = nullptr;
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dropped Sleev Bullet")
    //TSubclassOf<class ADroppableItemDefault> SleevBullet = nullptr;

    //// inv
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    //float SwitchTimeToWeapon = 1.0f;
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    //UTexture2D* WeaponItem = nullptr;
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    //EWeaponType WeaponType = EWeaponType::RifleType;

};

USTRUCT(BlueprintType)
struct FAddicionalWeaponInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispertion")
    int32 Round = 10;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
};