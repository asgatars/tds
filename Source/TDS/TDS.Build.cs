// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS : ModuleRules
{
    public TDS(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "NavigationSystem", "AIModule", "Niagara", "PhysicsCore", "Slate" });

        PublicIncludePaths.AddRange(new string[] { "TDS", "TDS/Character", "TDS/FunctionLibrary", "TDS/Game", "TDS/Weapon" });
    }
}
